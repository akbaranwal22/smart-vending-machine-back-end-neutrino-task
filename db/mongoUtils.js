const mongoose = require("mongoose");
const URL = "mongodb+srv://neutrino:neutrino123@cluster1.mzq3ixa.mongodb.net/?retryWrites=true&w=majority";


function mongooseConnect() {
    mongoose.connect(URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(response => {
        console.log("Mongoose Connected...")
    }).catch(err => console.log(err));
}

module.exports = { mongooseConnect };

