const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

const mongoUtils = require("./db/mongoUtils");
const {AdminModel} = require("./model/user-admi-login.model");
const { FoodProductModel } = require("./model/food-product.model")

app.use(cors());
app.use(bodyParser.json());

mongoUtils.mongooseConnect();

app.get("/admin/:username", (req, res) => {
    if (req.params) {
        const username = req.params.username;
        AdminModel.findOne({ username })
            .then(response => {
                return res.status(200).send( response );
            })
            .catch(err => console.log(err))
    }
})

app.post("/user-registration", (req, res) => {
    if (req.body) {
        const adminDetail = new AdminModel(req.body);
        adminDetail.save()
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err));
    }
})

app.post("/add-food-product", (req, res) => {
    if (req.body) {
        const addedFoodDetails = new FoodProductModel(req.body);
        addedFoodDetails.save()
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err));
    }
})

app.get("/add-food-product", (req, res) => {
    if (req.params) {
        FoodProductModel.find()
            .then(response => {
                return res.status(200).send( response );
            })
            .catch(err => console.log(err))
    }
})

  

app.listen(8000, () => {
    console.log("Server started at PORT 8000");
})
