const mongoose=require("mongoose");

const FoodProductSchema= new mongoose.Schema({
    foodName: {
        type: String,
        required: true
    },
    price : {
        type: Number,
        required: true
    },
    description:{
        type: String,
        required:true
    }
})

const FoodProductModel=mongoose.model("food",FoodProductSchema);

module.exports= {FoodProductModel};
