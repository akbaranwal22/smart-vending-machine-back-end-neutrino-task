const mongoose=require("mongoose");

const AdminSchema = new mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
});


const AdminModel= mongoose.model('admins', AdminSchema);

module.exports= {AdminModel};
